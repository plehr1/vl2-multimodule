# VL2 Multimodule

## Welche Vorteile bietet Git Flow im Allgemeinen?
Git Flow bietet den Vorteil von einer Organisation der geteilten Entwicklung. Jeder Entwickler weiß wann in welchem Branch entwickelt wird und pusht nicht versehentlich in zum Beispiel die Produktion.

## Warum braucht man Git Flow mehrheitlich in größeren Teams?
Wenn man alleine in einem Projekt arbeitet oder maximal zu zweit, kann man die Organisation durch Absprachen erledigen. Bei großen Teams könnten es bei gleichzeitigen Entwicklungen zu welchselseitigen Problemen kommen, wenn man zum Beispiel eine gemeinsame Datei bearbeitet hat.

## Welche Aufgaben/Verantwortlichkeiten seht ihr bei der Verwendung von Git Flow auf Seiten der Entwickler?
Die Verantwortung sehe ich klar in der Einhaltung der abgestimmten namensvergabe und an den Flow.